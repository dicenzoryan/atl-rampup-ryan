package router

import (
	"encoding/json"
	"fmt"
	"net/http"

	"../models"

	"github.com/gorilla/mux"
)

const (
	region = "us-west-1"
	bucket = "atlrampup"
)

// Router is exported and used in main.go
func Router() *mux.Router {
	router := mux.NewRouter()
	router.HandleFunc("/", GetCodebaseLogo).Methods(http.MethodGet)
	return router
}

// GetCodebaseLogo gets returns a json payload containing the URL for the club logo
func GetCodebaseLogo(w http.ResponseWriter, r *http.Request) {
	key := "codebaes.png"
	// Apparently getting an image URL is just constructing by hand lol
	output := models.Payload{
		ImageURL: fmt.Sprintf("https://s3-%s.amazonaws.com/%s/%s", region, bucket, key),
	}
	w.Header().Set("Content-Type", "application/json")
	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.WriteHeader(http.StatusOK)
	json.NewEncoder(w).Encode(output)
}
